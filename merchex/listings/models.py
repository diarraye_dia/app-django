from random import choices
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


class Band(models.Model):
    name = models.fields.CharField(max_length=100)
    genre = models.fields.CharField(max_length=50)
    biography = models.fields.CharField(max_length=1000)
    year_formed = models.fields.IntegerField(validators=[MinValueValidator(1900), MaxValueValidator(2022)])
    active = models.fields.BooleanField(default=True)
    official_homepage = models.fields.URLField(null=True, blank=True)

    class Genre(models.TextChoices):
        POP_ROCK = 'PR'
        INDIE = 'IN'
        SOUL = 'SO'
        ALTERNATIVE = 'AL'
    genre = models.fields.CharField(choices=Genre.choices, max_length=5)

    def __str__(self):
        return f'{self.name}'


class Listing(models.Model):
    title = models.fields.CharField(max_length=100)
    description = models.fields.CharField(max_length=1000)
    sold = models.fields.BooleanField(default=False)
    year = models.fields.IntegerField(validators=[MinValueValidator(1900), MaxValueValidator(2022)])
    band = models.ForeignKey(Band, null=True, on_delete=models.SET_NULL)
    

    class Type(models.TextChoices):
        RECORDS = 'Record'
        CLOTHING = 'Clothing'
        POSTERS = 'Poster'
        MISCELLANEOUS = 'Miscellaneous'
    type = models.fields.CharField(choices=Type.choices, max_length=100)

    def __str__(self):
        return f'{self.title}'