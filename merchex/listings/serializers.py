from rest_framework.serializers import ModelSerializer
from listings.models import Band, Listing


class BandSerializer(ModelSerializer):

    class Meta:
        model = Band
        fields = ['id', 'name']

class ListingSerializer(ModelSerializer):

    class Meta:
        model = Listing
        fields = ['id', 'title', 'sold']